import {getOr} from 'lodash/fp';
import Distrital from 'components/charts/Map/Distrital';
import {connect} from 'react-redux';
import {setProvincia, setDistrito} from 'actions';
import Layout from 'components/Layout';
import CandidateList from 'components/CandidateList';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  container: {
    display: 'flex',
  },
  map: {
    [theme.breakpoints.up('md')]: {
      flex: '1 1 50%',
    },
  },
  candidateList: {
    [theme.breakpoints.up('md')]: {
      flex: '1 1 50%',
    },
  }
});

const MapaDistrital = ({classes, ...props}) => (
  <Layout title="Distrito">
    <div className={classes.container}>
      <Distrital
        className={classes.map}
        height={550}
        styles={{
          colors: {
            light: '#BBDEFB',
            accent: '#1565C0',
          },
        }}
        {...props}
        />
    </div>
    <CandidateList className={classes.candidateList} ubigeo={props.ubigeo} />
  </Layout>
);

const ConnectedDistrital = connect(
  ({dashboard: {provincia, distrito}}) => ({
    ubigeo: getOr(null, 'ubigeo', provincia),
    selected: getOr(null, 'ubigeo', distrito),
  }),
  dispatch => ({
    onSelected: (distrito) => {
      const action = setDistrito(
        distrito ? {
          ubigeo: distrito.ubigeo,
          nombre: distrito.name,
        } : null,
      );
      return dispatch(action);
    },
  })
)(withStyles(styles)(MapaDistrital));

export default ConnectedDistrital
