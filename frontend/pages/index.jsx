import {getOr} from 'lodash/fp';
import Nacional from 'components/charts/Map/Nacional';
import {connect} from 'react-redux';
import {setDepartamento} from 'actions';
import { withRouter } from 'next/router'
import Zoom from '@material-ui/core/Zoom';
import Layout from 'components/Layout';

import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
  map: {
    maxWidth: 500,
    [theme.breakpoints.up('md')]: {
      width: '50%',
    },
  },
});

const MapaNacional = ({classes, ...props}) => (
  <Layout title="Departamento">
    <Zoom in>
      <Nacional
        className={classes.map}
        height={600}
        styles={{
          colors: {
            light: '#BBDEFB',
            accent: '#1565C0',
          },
        }}
        {...props}
        />
    </Zoom>
  </Layout>
);

const ConnectedNacional = connect(
  ({dashboard: {departamento}}) => ({
    selected: getOr(null, 'ubigeo', departamento),
  }),
  dispatch => ({
  onSelected: (departamento) => {
    const action = setDepartamento(
      departamento ? {
        ubigeo: departamento.ubigeo,
        nombre: departamento.name,
      } : null,
    );
    return dispatch(action);
  },
  }))(withStyles(styles)(MapaNacional));

export default ConnectedNacional;
