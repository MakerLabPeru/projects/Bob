import {getOr} from 'lodash/fp';
import Provincial from 'components/charts/Map/Provincial';
import {connect} from 'react-redux';
import {setProvincia} from 'actions';
import Layout from 'components/Layout';
import CandidateList from 'components/CandidateList';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  container: {
    display: 'flex',
  },
  map: {
    [theme.breakpoints.up('md')]: {
      flex: '1 1 50%',
    },
  },
  candidateList: {
    [theme.breakpoints.up('md')]: {
      flex: '1 1 50%',
    },
  }
});

const MapaProvincial = ({classes, ...props}) => (
  <Layout title="Provincia">
    <div className={classes.container}>
      <Provincial
        className={classes.map}
        height={550}
        styles={{
          colors: {
            light: '#BBDEFB',
            accent: '#1565C0',
          },
        }}
        {...props}
        />
      <CandidateList className={classes.candidateList} ubigeo={props.ubigeo} />
    </div>
  </Layout>
);

const ConnectedProvincial = connect(
  ({dashboard: {departamento, provincia}}) => ({
    ubigeo: getOr(null, 'ubigeo', departamento),
    selected: getOr(null, 'ubigeo', provincia),
  }),
  dispatch => ({
    onSelected: (provincia) => {
      const action = setProvincia(
        provincia ? {
          ubigeo: provincia.ubigeo,
          nombre: provincia.name,
        } : null,
      );
      return dispatch(action);
    },
  })
)(withStyles(styles)(MapaProvincial));

export default ConnectedProvincial;
