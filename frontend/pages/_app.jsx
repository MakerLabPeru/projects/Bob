// pages/_app.js
import React from "react";
import {compose} from 'recompose';
import {createStore} from "redux";
import {Provider as ReduxProvider} from "react-redux";
import App, {Container} from "next/app";
import withRedux from "next-redux-wrapper";
import withReduxSaga from 'next-redux-saga'
import initStore from 'flux/store';

import { MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import JssProvider from 'react-jss/lib/JssProvider';
import getPageContext from 'lib/page-context';


class MyApp extends App {
  constructor(props) {
    super(props);
    this.pageContext = getPageContext();
  }
  pageContext = null;

  componentDidMount() {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }

  static async getInitialProps({Component, ctx}) {
    const pageProps = Component.getInitialProps ?
      await Component.getInitialProps(ctx) : {};
    return {pageProps};
  }

  render() {
    const { Component, pageProps, store } = this.props;
    return (
      <Container>
        <JssProvider
          registry={this.pageContext.sheetsRegistry}
          generateClassName={this.pageContext.generateClassName}
          >
          {/* MuiThemeProvider makes the theme available down the React
          tree thanks to React context. */}
          <MuiThemeProvider
            theme={this.pageContext.theme}
            sheetsManager={this.pageContext.sheetsManager}
            >
            {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
            <CssBaseline />
            {/* Pass pageContext to the _document though the renderPage enhancer
            to render collected styles on server side. */}
            <ReduxProvider store={store}>
              <Component pageContext={this.pageContext} {...pageProps} />
            </ReduxProvider>
          </MuiThemeProvider>
        </JssProvider>
      </Container>
    );
  }

}

export default compose(
  withRedux(initStore),
  withReduxSaga,
)(MyApp);
