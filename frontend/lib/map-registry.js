import {map, filter, padCharsEnd, trim, trimCharsEnd, compose, curryN} from 'lodash/fp';
import {arcgisToGeoJSON} from '@esri/arcgis-to-geojson-utils';
import fetchPonyfill from 'fetch-ponyfill';
import localforage from 'localforage';
import {echarts} from 'components/charts/echarts';

const {fetch} = fetchPonyfill();

const registry = localforage.createInstance({
  name: 'minem-maps',
});

const mapServerBaseUrl = [
  'http://geoportal.minem.gob.pe/minem/rest/services/WMS',
  'MAPA_PERU_LIMITE_WGS84/MapServer',
].join('/');

const processArcgis = (arcgisJson) => {
  const geoJson = arcgisToGeoJSON(arcgisJson);
  const {
    DEPARTAMEN,
    DISTRITO,
    PROVINCIA,
    ID_UBI: ubigeo,
  } = geoJson.properties;
  const [departamento, provincia, distrito] = map(trim, [
    DEPARTAMEN, PROVINCIA, DISTRITO,
  ]);
  const properties = {
    departamento,
    distrito,
    provincia,
    name: distrito || provincia || departamento,
    ubigeo: padCharsEnd(0, 6)(ubigeo),
  };
  return {
    ...geoJson,
    properties,
  };
};

const createMapQuery = curryN(3)((layer, where, precision) => {
  console.log('MapQuery:', layer, where, precision);
  const path = `${mapServerBaseUrl}/${layer}/query`;
  const query = [
    'f=json',
    'outfields=ID_UBI,DEPARTAMEN,DISTRITO,PROVINCIA',
    `geometryPrecision=${precision}`,
    `where=${where}`,
  ].join('&');
  return `${path}?${query}`;
});

// Descarga un mapa del servidor ArcGis
const requestMap = layer => where => async (precision) => {
  const query = createMapQuery(layer, where, precision)
  const res = await fetch(query);
  const resJson = await res.json();
  return map(processArcgis, resJson.features);
};

const cacheGeoJson = (geo) => {
  const {ubigeo} = geo.properties;

  return registry.setItem(ubigeo, geo);
};

const cacheGeoJsonArray = compose(
  Promise.all.bind(Promise), // Curiosamente, esto es necesario
  map(cacheGeoJson),
);

// Convierte un array de geoJson a un geoJson de tipo 'FeatureCollection'
export const geoJsonArrayToCollection = (geoArray, rest) => ({
  type: 'FeatureCollection',
  features: geoArray,
  ...rest,
});

const requestAllDepartamentos = () => requestMap(2)(
  'ID_UBIGEO is not NULL',
)(2);

const requestProvinciasOfDepartamento = ubigeoDep => requestMap(1)(
  `IDDPTO=${trimCharsEnd('0', ubigeoDep)}`,
)(2);

const requestDistritosOfDepartamento = ubigeoDep => requestMap(0)(
  `IDDPTO=${trimCharsEnd('0', ubigeoDep)}`,
)(4);

const requestDistritosOfProvincia = ubigeoProv => requestMap(0)(
  `IDPROV=${trimCharsEnd('0', ubigeoProv)}`,
)(4);


// Devuelte una promesa por la lista de geoJson respectiva a lista de ubigeos
const resolveUbigeos = ubigeos => compose(
  Promise.all.bind(Promise),
  map(registry.getItem.bind(registry)),
)(ubigeos);

const getFilteredOrRequest = async (filterIterator, reqFunc) => {
  const ubigeos = filter(filterIterator)(await registry.keys());

  if (ubigeos.length) {
    return resolveUbigeos(ubigeos);
  }

  const res = await reqFunc();
  await cacheGeoJsonArray(res);

  return res;
};

export const getAllDepartamentos = () => getFilteredOrRequest(
  ubigeo => ubigeo.slice(2) === '0000',
  requestAllDepartamentos,
);

export const getProvinciasOfDepartamento = ubigeoDep => getFilteredOrRequest(
  ubigeo => ubigeo.slice(4) === '00' && ubigeo.slice(2, 4) !== '00'
    && ubigeo.slice(0, 2) === ubigeoDep.slice(0, 2),
  () => requestProvinciasOfDepartamento(ubigeoDep),
);

export const getDistritosOfDepartamento = ubigeoDep => getFilteredOrRequest(
  ubigeo => ubigeo.slice(4) !== '00' &&
    ubigeo.slice(0, 2) === ubigeoDep.slice(0, 2),
  () => requestDistritosOfDepartamento(ubigeoDep),
);

export const getDistritosOfProvincia = ubigeoProv => getFilteredOrRequest(
  ubigeo => ubigeo.slice(4) !== '00' &&
    ubigeo.slice(0, 4) === ubigeoProv.slice(0, 4),
  () => requestDistritosOfProvincia(ubigeoProv),
);

if (process.browser) {
  window.echarts = echarts;
  window.createMapQuery = createMapQuery;
  window.MapRegistry = registry;
  window.getAllDepartamentos = getAllDepartamentos;
  window.getProvinciasOfDepartamento = getProvinciasOfDepartamento;
  window.getDistritosOfDepartamento = getDistritosOfDepartamento;
}
