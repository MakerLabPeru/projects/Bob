import {map, filter, padCharsEnd, trim, trimCharsEnd, compose, curryN} from 'lodash/fp';
import {arcgisToGeoJSON} from '@esri/arcgis-to-geojson-utils';
import fetchPonyfill from 'fetch-ponyfill';
import localforage from 'localforage';
import {echarts} from 'components/charts/echarts';

const {fetch} = fetchPonyfill();

const registry = localforage.createInstance({
  name: 'candidatos',
});

const candidatosUrl = (ubigeo) => `http://192.168.2.102:8000/candidatos/${ubigeo}`;

const requestCandidatos = async (ubigeo) => {
  const res = await fetch(candidatosUrl(ubigeo));
  return res.json();
}

const cacheCandidatos = (candidatos, ubigeo) => {
  return registry.setItem(ubigeo, {date: Date.now(), candidatos});
}

export const getCandidatos = async (ubigeo) => {
  const res = await registry.getItem(ubigeo);
  if(res === null) {
    return requestCandidatos(ubigeo);
  }
};

if (process.browser) {
  window.CandidatosRegistry = registry;
}
