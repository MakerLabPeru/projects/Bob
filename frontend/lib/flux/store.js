import {createStore, applyMiddleware, combineReducers} from 'redux';
import createSagaMiddleware from 'redux-saga'
import {createLogger} from 'redux-logger';
import {composeWithDevTools} from 'redux-devtools-extension';
import reducerObj from 'flux/reducers';
import rootSaga from 'flux/sagas';

const reducer = combineReducers(reducerObj);

const sagaMiddleware = createSagaMiddleware();

const middlewareArray = [
  sagaMiddleware,
];

if (process.env.NODE_ENV !== 'production') {
  const logger = createLogger({
    level: 'info',
    collapsed: true,
  });

  middlewareArray.push(logger);
}

const middleware = applyMiddleware(...middlewareArray);
const enhancer = composeWithDevTools({})(middleware);


export default (initialState = {}) => {
  const store = createStore(
    reducer,
    initialState,
    enhancer,
  );

  store.runSagaTask = () => {
    store.sagaTask = sagaMiddleware.run(rootSaga)
  }

  store.runSagaTask();

  return store;
};
