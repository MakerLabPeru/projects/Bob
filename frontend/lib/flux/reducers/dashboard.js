import {
  SET_DEPARTAMENTO,
  SET_PROVINCIA,
  SET_DISTRITO,
} from 'actions';

const initialState = {
  departamento: null,
  provincia: null,
  distrito: null,
};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case SET_DEPARTAMENTO:
      return {
        ...state,
        departamento: payload.departamento,
        provincia: null,
        distrito: null,
      };
    case SET_PROVINCIA:
      return {
        ...state,
        provincia: payload.provincia,
        distrito: null,
      };
    case SET_DISTRITO:
      return {
        ...state,
        distrito: payload.distrito,
      };
    default:
      return state;
  }
};
