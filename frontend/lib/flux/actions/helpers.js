import Case from 'case';

// Function that just returns null
const noop = () => null;

export function actionFactory(actionType, payloadFactory = noop) {
  // Verify that the actionType is in constant case
  if (Case.of(actionType) !== 'constant') {
    throw Error(`Name of action '${actionType}' must be in constant case`);
  }

  const factory = data => ({
    type: actionType,
    payload: payloadFactory(data),
  });
  // Change the factory name for debugging purposes
  Object.defineProperty(factory, 'name', {
    value: Case.camel(actionType),
    writable: false,
  });

  return factory;
}
