import {actionFactory} from 'actions/helpers';

export const SET_DEPARTAMENTO = 'SET_DEPARTAMENTO';
export const SET_PROVINCIA = 'SET_PROVINCIA';
export const SET_DISTRITO = 'SET_DISTRITO';


export const setDepartamento = actionFactory(
  SET_DEPARTAMENTO,
  (departamento) => {
    if (!departamento) {
      return {departamento: null};
    }
    const {ubigeo, nombre} = departamento;
    return ({departamento: {ubigeo, nombre}});
  },
);

export const setProvincia = actionFactory(
  SET_PROVINCIA,
  (provincia) => {
    if (!provincia) {
      return {provincia: null};
    }
    const {ubigeo, nombre} = provincia;
    return ({provincia: {ubigeo, nombre}});
  },
);

export const setDistrito = actionFactory(
  SET_DISTRITO,
  (distrito) => {
    if (!distrito) {
      return {distrito: null};
    }
    const {ubigeo, nombre} = distrito;
    return ({distrito: {ubigeo, nombre}});
  },
);
