import {get} from 'lodash/fp';
import Router from 'next/router';

import {
  SET_DEPARTAMENTO,
  SET_PROVINCIA,
  SET_DISTRITO,
} from 'flux/actions'

import {
  takeEvery,
  throttle,
  select,
} from 'redux-saga/effects';


function* routeSaga() {
  yield throttle(200, [
    SET_DEPARTAMENTO,
    SET_PROVINCIA,
    SET_DISTRITO,
  ], navigateUser);
}

function* navigateUser({type, payload}) {
  switch(type) {
    case SET_DEPARTAMENTO:
      payload.departamento
          ? Router.push({
            pathname: '/provincial',
          })
      : Router.push({
        pathname: '/',
      });
      break;
    case SET_PROVINCIA:
      payload.provincia
          ? Router.push({
            pathname: '/distrital',
          })
      : Router.push({
        pathname: '/',
      });
      break;
    case SET_DISTRITO:
      if(! payload.distrito) {
        Router.push({
          pathname: '/provincial',
        })
      }
  }
}

export default routeSaga;
