import {
  fork,
  all,
} from 'redux-saga/effects';

import routeSaga from './route';

export default function* rootSaga() {
  yield all([
    fork(routeSaga),
  ]);
}
