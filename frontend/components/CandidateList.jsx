import {Component} from 'react';
import {map} from 'lodash/fp';
import { withStyles } from '@material-ui/core/styles';
import CandidateCard from './CandidateCard';
import {getCandidatos} from 'lib/candidatos-registry';


const CandidateList = ({className, candidatos}) => {
  const cards = map(
    (candidato) => <CandidateCard candidato={candidato} />
  )(candidatos);

  return (
    <div className={className}>
      {cards}
    </div>
  );
}


const CandidateListContainer = (Wrapped) => {
  class Wrapper extends Component {
    constructor(props) {
      super(props);

      this.state = {
        loadingData: false,
        data: null,
      }
    }

    componentDidMount() {
      this.loadData();
    }

    async loadData() {
      const {state} = this;
      if (state.loadingData || state.data !== null) {
        return;
      }
      this.setState({loadingData: true});
      const data = await getCandidatos(this.props.ubigeo);
      this.setState({data, loadingData: false});
    }

    render() {
      const {data, loadingData} = this.state;
      return (
        <Wrapped candidatos={data} loading={loadingData} {...this.props} />
      );
    }
  }

  return Wrapper;
}


export default CandidateListContainer(CandidateList);
