import {map} from 'lodash/fp'
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  card: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 120,
    height: 120,
  },
});

const CandidateCard = ({classes, candidato}) => {
  const {articles, name, org_polit, url_photo} = candidato;

  const ArticleComps = map(({url, title, summary}) => (
    <div>
      <a href={url}>
        <Typography variant="subheading">
          {title}
        </Typography>
      </a>
      <Typography variant="caption">
        {summary}
      </Typography>
    </div>
  ));

  return (
    <div>
      <Card className={classes.card}>
        <div className={classes.details}>
          <CardContent className={classes.content}>
            <Typography variant="headline">{name}</Typography>
            <Typography variant="subheading" color="textSecondary">
              {org_polit}
            </Typography>
          </CardContent>
        </div>
        <CardMedia
          className={classes.cover}
          image={url_photo}
        />
      </Card>
      <div>
        {articles.length ? ArticleComps :
          <Typography variant="caption">
              No se encontraron articulos
        </Typography>}
      </div>
    </div>
  );
};

export default withStyles(styles)(CandidateCard);
