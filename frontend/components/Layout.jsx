import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const styles = {
  root: {
    flexGrow: 1,
  },
};

const Layout = ({classes, title, children}) => {
  return (
    <div>
      <AppBar position="static">
        <Toolbar>
        </Toolbar>
      </AppBar>
      <AppBar>
        <Toolbar>
          <Typography variant="title" color="inherit">
            {title}
          </Typography>
        </Toolbar>
      </AppBar>
      {children}
    </div>
  );
}

export default withStyles(styles)(Layout);
