import {get, map, max, compose} from 'lodash/fp';

const seriesDataFromProps = ({
  selected: selectedUbigeo,
  styles,
}) => {
  return map(({ubigeo, ...rest}) => ({
    ubigeo,
    selected: selectedUbigeo === ubigeo,
  }))(data);
};

export const seriesFromProps = ({styles, mapName, selected}) => ({
  map: mapName,
  type: 'map',
  roam: false,
  selectedMode: 'single',
  itemStyle: {
    opacity: 0,
    borderWidth: 2,
    borderColor: get('colors.light', styles),
    emphasis: {
      label: {
        show: true,
      },
      areaColor: null,
    },
  },
  label: {
    show: false,
  },
});
