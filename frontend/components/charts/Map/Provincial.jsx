/*
   Mapa de un departamento
 */
import PropTypes from 'prop-types';
import {wrap} from 'lodash/fp';
import {
  getProvinciasOfDepartamento as getProvincias,
} from 'lib/map-registry';
import Map from './Map';

const MapaProvincial = ({ubigeo, ...rest}) => {
  const mapLoader = wrap(getProvincias, ubigeo);
  const mapName = `${ubigeo}:provincias`;
  return (
    <Map
      mapName={mapName}
      mapLoader={mapLoader}
      {...rest}
    />
  );
};

MapaProvincial.propTypes = {
  ubigeo: PropTypes.string.isRequired,
  conDistritos: PropTypes.bool,
};

MapaProvincial.defaultProps = {
  conDistritos: false,
};

export default MapaProvincial;
