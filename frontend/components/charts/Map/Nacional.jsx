import {getAllDepartamentos} from 'lib/map-registry';
import Map from './Map';

const MapaNacional = props => (
  <Map
    mapName="peru"
    mapLoader={getAllDepartamentos}
    {...props}
  />
);

export default MapaNacional;