import PropTypes from 'prop-types';
import {Component} from 'react';
import {find, get, isNaN, compose, noop, map, split, capitalize} from 'lodash/fp';
import EchartsReact, {echarts} from 'components/charts/echarts';
import {geoJsonArrayToCollection} from 'lib/map-registry';


const optionsFromProps = ({
  mapName, title, styles, selected, ...rest
}) => ({
  title: {
    text: title,
  },
  tooltip: {
    trigger: 'item',
    backgroundColor: get('colors.light', styles),
    textStyle: {
      color: get('colors.accent', styles),
    },
    formatter: compose(
      map(capitalize),
      split(' '),
      get('name'),
    ),
  },
  series: [
    {
      map: mapName,
      type: 'map',
      top: 20,
      roam: 'scale',
      scaleLimit: {
        min: 0.8,
        max: 2.5,
      },
      selectedMode: 'single',
      data: compose(
        map(({name, ubigeo: value}) => ({name, value, selected: value == selected})),
        map('properties'),
        get('geoJson.features'),
      )(echarts.getMap(mapName)),
      itemStyle: {
        areaColor: '#CFD8DC',
        borderColor: '#FFFFFF',
        borderWidth: 2,
        emphasis: {
          opacity: 1,
          color: get('colors.light', styles),
          borderWidth: 2,
          borderColor: get('colors.accent', styles),
          areaColor: null,
          label: {
            show: false,
          },
        },
      },
      label: {
        show: false,
      },
    }
  ],
});

class Map extends Component {
  static getDerivedStateFromProps(nextProps) {
    const hasMap = !!echarts.getMap(nextProps.mapName);
    return {
      mapAvailable: hasMap,
    };
  }
  constructor(props) {
    super(props);
    this.onChartReady = this.onChartReady.bind(this);
    this.onMouseOver = this.onMouseOver.bind(this);
    this.onMouseOut = this.onMouseOut.bind(this);
    this.onSelected = this.onSelected.bind(this);
  }
  state = {
    // Indica si se esta cargando el mapa
    loadingMap: false,
  }
  componentDidMount() {
    this.ensureMap(this.props.mapName);
  }
  componentDidUpdate() {
    this.ensureMap(this.props.mapName);
  }
  //
  // Event handlers
  //
  onChartReady(chart) {
    this.chart = chart;
    this.ensureMap();
  }
  onSelected({batch}) {
    const {mapName, onSelected} = this.props;
    const {name, selected: selectedObj} = batch[0];
    const selected = get(name, selectedObj);

    if (!selected) {
      return onSelected(null);
    }

    const {geoJson} = echarts.getMap(mapName);

    const ubigeo = compose(
      get('properties.ubigeo'),
      find(({properties}) => properties.name === name),
      get('features'),
    )(geoJson);

    if (ubigeo) {
      onSelected({ubigeo, name});
    }
  }
  onMouseOver({data}) {
    const {onMouseOver} = this.props;
    if (data && onMouseOver) {
      onMouseOver(data.ubigeo);
    }
  }
  onMouseOut({data}) {
    const {onMouseOut} = this.props;
    if (data && onMouseOut) {
      onMouseOut(data.ubigeo);
    }
  }
  //
  // Map loading methods
  //
  ensureMap() {
    const {loadingMap, mapAvailable} = this.state;
    if (!mapAvailable && !loadingMap) {
      this.setState({loadingMap: true});
      this.registerMap();
    }
  }
  async registerMap() {
    const {mapLoader, mapName} = this.props;

    const geo = await mapLoader();
    echarts.registerMap(mapName, geoJsonArrayToCollection(geo));
    this.setState({mapAvailable: true, loadingMap: false});
  }

  //
  // Render method
  //
  render() {
    const {height, containerStyle, className} = this.props;
    const {mapAvailable, loadingMap} = this.state;
    return (
      <div className={className} style={containerStyle}>
        <EchartsReact
          onChartReady={this.onChartReady}
          showLoading={!mapAvailable || loadingMap}
          option={loadingMap ? {} : optionsFromProps(this.props)}
          style={{
            height,
          }}
          onEvents={{
            mapselectchanged: this.onSelected,
            mouseover: this.onMouseOver,
            mouseout: this.onMouseOut,
          }}
          notMerge
          />
      </div>
    );
  }
}

Map.propTypes = {
  /* eslint-disable react/no-unused-prop-types */
  // Titulo de la grafica,
  title: PropTypes.string,
  // Altura de la grafica
  height: PropTypes.number.isRequired,
  // Eventos
  onMouseOver: PropTypes.func,
  onMouseOut: PropTypes.func,
  onSelected: PropTypes.func,
  // Funcion que se llama (sin parametros) para obtener el mapa
  // (array de geoJson)
  mapLoader: PropTypes.func.isRequired,
  // Nombre con el que se registra el mapa
  mapName: PropTypes.string.isRequired,
  styles: PropTypes.shape({
    colors: PropTypes.shape({
      light: PropTypes.string,
      primary: PropTypes.string,
      accent: PropTypes.string,
    }),
  }),
  /* eslint-enable */
};

Map.defaultProps = {
  onMouseOver: noop,
  onMouseOut: noop,
  onSelected: noop,
  styles: {},
  title: '',
};

export default Map;
