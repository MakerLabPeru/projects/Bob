/*
  Mapa de una provincia
*/

import PropTypes from 'prop-types';
import {wrap} from 'lodash/fp';
import {
  getDistritosOfProvincia as getDistritos,
} from 'lib/map-registry';
import Map from './Map';


const MapaDistrital = ({ubigeo, ...rest}) => (
  <Map
    mapName={`${ubigeo}:distritos`}
    mapLoader={wrap(getDistritos, ubigeo)}
    {...rest}
    />
);

MapaDistrital.propTypes = {
  ubigeo: PropTypes.string.isRequired,
};

export default MapaDistrital;
