import map from 'lodash/fp/map';
import get from 'lodash/fp/get';
import first from 'lodash/fp/first';
import compose from 'lodash/fp/compose';

export const seriesFromDataProp = map(
  ({name, data: seriesData}) => ({
    name,
    type: 'line',
    stack: 'default',
    areaStyle: {normal: {}},
    data: map(get('y'))(seriesData),
  }),
);

export const xAxisDataFromDataProp = compose(
  map(get('x')),
  get('data'),
  first,
);
