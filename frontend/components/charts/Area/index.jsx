import {Component} from 'react';
import PropTypes from 'prop-types';
import {get, getOr, map, compose, noop} from 'lodash/fp';
import Echarts from 'components/charts/echarts';
import {
  xAxisDataFromDataProp,
  seriesFromDataProp,
} from './data';


const optionsFromProps = ({
  title, data, styles, withBrush, xAxisLabel, yAxisLabel,
}) => ({
  title: {
    text: title,
  },
  brush: withBrush ? {
    xAxisIndex: 'all',
    throttleType: 'debounce',
    throttleDelay: 50, // ms
  } : {},
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'cross',
      label: {
        backgroundColor: getOr(
          '#6a7985', 'tooltip.label.backgroundColor', styles,
        )
        ,
      },
    },
  },
  xAxis: {
    type: 'category',
    name: xAxisLabel,
    nameLocation: 'center',
    boundaryGap: false,
    data: xAxisDataFromDataProp(data),
    nameRotate: 0,
    nameGap: 30,
  },
  yAxis: {
    type: 'value',
    name: yAxisLabel, // Comportamiento extraño de echarts
    nameLocation: 'center',
    nameGap: 30,
    nameRotate: 90,
  },
  series: seriesFromDataProp(data),
});


class AreaChart extends Component {
  constructor(props) {
    super(props);
    this.handleBrush = this.handleBrush.bind(this);
  }
  handleBrush({batch}) {
    const {onBrush} = this.props;
    const {chart} = this;
    if (!chart) {
      return;
    }
    const chartOpts = chart.getOption();

    const firstYear = get('xAxis.0.data.0', chartOpts);

    const yearRange = compose(
      map(v => firstYear + v),
      get('0.areas.0.coordRange'),
    )(batch);

    console.log('Brushed:', yearRange);

    onBrush(yearRange);
  }
  render() {
    const {height, onBrush, style, ...rest} = this.props;

    return (
      <Echarts
        onChartReady={(e) => { this.chart = e; }}
        option={optionsFromProps(rest)}
        style={{...style, height}}
        onEvents={{
          brushselected: onBrush ? this.handleBrush : noop,
        }}
        {...rest}
      />
    );
  }
}

AreaChart.defaultProps = {
  onDataZoom: noop,
  style: {},
  withBrush: false,
  onBrush: noop,
};

AreaChart.propTypes = {
  style: PropTypes.objectOf(PropTypes.string),
  height: PropTypes.number.isRequired,
  onDataZoom: PropTypes.func,
  withBrush: PropTypes.bool,
  onBrush: PropTypes.func,
};

export default AreaChart;