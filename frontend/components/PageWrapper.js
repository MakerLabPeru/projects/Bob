/* -*- mode: react -*- */
import PropTypes from 'prop-types';
import {Component} from 'react';
import {getOr, compose} from 'lodash/fp';
import {wrapDisplayName, hoistStatics} from 'recompose';
import initStore from 'flux/store';
import withRedux from 'next-redux-wrapper';


// Componente de orden superior para envolver las páginas
const PageWrapperHOC = (Page) => {
  /* eslint-disable react/prefer-stateless-function */
  class PageWrapper extends Component {
    render() {
      return (
        <Page {...this.props} />
      );
    }
  }
  /* eslint-enable */
  if (process.env.NODE_ENV !== 'production') {
    PageWrapper.displayName = wrapDisplayName(Page, 'PageWrapper');
  }

  return compose(
    withRedux(initStore),
  )(PageWrapper);
};

export default hoistStatics(PageWrapperHOC);
