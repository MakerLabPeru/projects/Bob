import scrapy
from scrapy import Request
from arrow import Arrow
from ..items import Article


class PoliticaSpider(scrapy.Spider):
    name = "politica"

    start_urls = [
        'https://elcomercio.pe/archivo/politica',
    ]

    # def __init__(self, start_date, end_date):
    #     self.start = start_date
    #     self.end = end_date

    def start_requests(self, today=Arrow.now()):
        fecha = today.replace(days=-1)
        while fecha > Arrow(2010, 1, 1):
            url = 'https://elcomercio.pe/archivo/politica/{}'.format(
                fecha.format('YYYY-MM-DD'))
            yield Request(url, callback=self.parse_archive, meta=dict(date=fecha))
            fecha = fecha.replace(days=-1)

    def parse_archive(self, response):
        items = response.xpath("//*[contains(@class,'column-flows')]/article")

        for item in items:
            title = item.xpath(".//*[contains(@class, 'flow-title')]/a/text()").extract_first()
            link = item.xpath(".//*[contains(@class, 'flow-title')]/a/@href").extract_first()
            summary = item.xpath(".//*[contains(@class, 'flow-summary')]/text()").extract_first()
            thumb_image = item.xpath(".//*[contains(@class, 'flow-image')]//img/@data-src").extract_first()
            yield response.follow(link, callback=self.parse_article, meta=dict(
                id=int(link.split('-')[-1]),
                title=title.strip(),
                date=response.meta['date'],
                summary=summary.strip(),
                thumb_image=thumb_image,))

    def parse_article(self, response):
        meta = response.meta
        text = '\n'.join(response.xpath('//*[contains(@class, "news-text")]//p/text()')
                         .extract())

        yield Article(
            id=meta['id'],
            url = response.url,
            title=meta['title'],
            date=meta['date'],
            summary=meta['summary'],
            thumb_image=meta['thumb_image'],
            content=text,
            source="el_comercio",
        )
