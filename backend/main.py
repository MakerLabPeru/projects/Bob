from flask import Flask, request, jsonify
from request import candidates_json
from flask_cors import CORS


app = Flask(__name__)

CORS(app)

@app.route("/candidatos/<ubigeo>")
def hello(ubigeo):
	ubigeo = ubigeo[:6]
	assert ubigeo.isnumeric()
	res = candidates_json(ubigeo)
	return jsonify(res)

# debug=True, se encuentra atento a cambios
# cambio de puerto a 8000
if __name__ == "__main__":
	app.run(debug=True, host="0.0.0.0", port=8000)