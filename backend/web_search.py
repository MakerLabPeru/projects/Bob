from elasticsearch import Elasticsearch
es = Elasticsearch(HOST="http://localhost", PORT=9200)
ID = 0

"""CAMPOS DE DOCUMENTOS GUARDADOS(KEYS)"""
idNew       = "id"
urlNew      = "url"             # devolver
title       = "title"           # devolver
date        = "date"
summary     = "summary"         # devolver
urlImage    = "thumb_image"     # devolver
content     = "content"
newspaper   = "source"

# index de datos obtenidos por scrapping
indexName = "bob"

'''
FUNCION: RETORNA LISTA DE DICCIONARIOS COMO RESULTADO DE UN QUERY
(es necesario saber la estructura de las respuestas)
'''
def searchData(indexName, name, cant=100):
    print(name[1].split(' ')[0])
    res = es.search(index=indexName, body={
        "size" : cant, # cantidad de noticias
        "query": {
            "bool": {
                # busqueda en tres zonas de las noticias recolectadas
                "should": [
                    { "match_phrase": { title   : name[1].split(' ')[0] + ' ' + name[0] }},
                    { "match_phrase": { summary : name[1].split(' ')[0] + ' ' + name[0] }},
                    { "match_phrase": { content : name[1].split(' ')[0] + ' ' + name[0] }}    
                ]
            }
        }
    })
    # ejemplo de la estructura de "res" al final del codigo
    # si se desea solo los datos insertados, usar campo "_source"
    return res["hits"]["hits"]


'''
FUNCION: retorna una lista de diccionarios
(elementos del diccionario son articulos de un candidato)
'''
# PREGUNTAR POR "_source", LUEGO...
# => urlNew
# => title
# => summary
# => urlImage
# printAnswer(answers)

def searchArticles(name):
    answers   = searchData(indexName, name, 10) 
    articles = []
    for data in answers:
        article = {}
        data = data["_source"]
        for key in [urlNew, title, summary, urlImage]:
            article[key] = data[key]
        articles.append(article)        
    return articles

'''
def printAnswer(answers):
    print("===".center(100,'='))
    print("RESULTADOS DE BUSQUEDA".center(100,'='))
    print("===".center(100,'='))
    for data in answers:
        data = data["_source"]
        print(data[title].upper().center(100,'-'))
        print("URL: " + data[urlNew])
        print("Summary: " + data[summary])
        print("Image: " + data[urlImage])
        print("===".center(100,'='))
'''
'''
ESTRUCTURA DE RESPUESTA A UN QUERY
{
  "took": 1,
  "timed_out": false,
  "_shards": {
    "total": 5,
    "successful": 5,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": 1402,
    "max_score": 27.449516,
    "hits": [
      {
        "_index": "bob",
        "_type": "articles",
        "_id": "5b663fbacc98672517ebc3ba",
        "_score": 27.449516,
        "_source": {
          "id": 526642,
          "url": "https://elcomercio.pe/politica/luis-castaneda-pardo-condicion-hijo-me-desmerece-noticia-526642",
          "title": "Luis Casta\u00f1eda Pardo: \u201cLa condici\u00f3n de hijo no me desmerece\u201d",
          "date": "2018-06-11T00:04:03.626000",
          "summary": "El hijo del burgomaestre Luis Casta\u00f1eda y candidato a la Alcald\u00eda de Lima por Solidaridad Nacional afirma que proteger\u00e1 a la municipalidad y al dinero de la gente",
          "thumb_image": "https://img.elcomercio.pe/files/listing_ec_seccion_separador_fotos/uploads/2018/06/10/5b1d55cb88a05.jpeg",
          "content": ", candidato a la Alcald\u00eda de Lima, calific\u00f3 de \u201cgolpe bajo\u201d las cr\u00edticas que han surgido en torno a su postulaci\u00f3n al sill\u00f3n municipal lime\u00f1o a ra\u00edz de su condici\u00f3n de hijo del actual burgomaestre de la capital, \n.\n\u201cCreo que la condici\u00f3n de hijo no me desmerece, por el contrario, me fortalece, porque he tenido una experiencia de vida de ver lo que ha sido de cerca una gesti\u00f3n durante 12 a\u00f1os con \u00e9l, luego, seis a\u00f1os yo como regidor\u201d, manifest\u00f3.\n indic\u00f3 que ha \u201ctrabajado a la interna\u201d de su partido, Solidaridad Nacional, para lograr tener su posici\u00f3n actual \u201csin dejar de incentivar la creaci\u00f3n de otros cuadros\u201d.\n\u201cEn el siguiente periodo electoral, si el marco jur\u00eddico se mantiene igual, yo ya no podr\u00eda optar por la reelecci\u00f3n, y en ese caso me encantar\u00eda tener otro cuadro, joven, con energ\u00eda, pero tambi\u00e9n muy profesional, al que pueda ceder la posta\u201d, se\u00f1al\u00f3 al diario \u201cCorreo\u201d.\nSostuvo que se encargar\u00e1 de proteger el dinero de la ciudadan\u00eda en el marco de la lucha anticorrupci\u00f3n y evitar\u00e1 que se vuelvan a repetir casos como el de Comunicore.\n\u201cPor supuesto, no se va a repetir, tengo una firmeza y convicci\u00f3n absoluta en la lucha contra la corrupci\u00f3n [\u2026] Lo que digo es que yo voy a proteger a la municipalidad y el dinero de la gente\u201d, expres\u00f3.\nComo se recuerda, en el Caso Comunicore se absolvi\u00f3 a 14 ex funcionarios de la Municipalidad de Lima que eran investigados por el presunto desembolso irregular de S/35,9 millones a la empresa Comunicore durante la primera gesti\u00f3n de Casta\u00f1eda Lossio. Este fue excluido del proceso por el Poder Judicial en el 2013.\nevit\u00f3 ahondar en el tema y dijo que \u201cno es qui\u00e9n para condenar o no la honra de muchas personas\u201d.\n\u201cComunicore fue un tema que se investig\u00f3 en su momento, fruto de la investigaci\u00f3n se constat\u00f3 de que mi padre no ten\u00eda responsabilidad y tambi\u00e9n se constat\u00f3 que no hubo perjuicio a los fondos municipales\u201d, acot\u00f3.\nLuis Casta\u00f1eda Pardo es administrador, tiene 32 a\u00f1os, fue electo regidor por La Molina en el 2006 y tambi\u00e9n result\u00f3 elegido como regidor en Lima Metropolitana en el 2010, pero fue uno de los revocados en el 2013 durante la gesti\u00f3n de Susana Villar\u00e1n.\nMIRA TAMBI\u00c9N EN POL\u00cdTICA...\nEl Comercio-Ipsos: todos los cuadros de la encuesta de junio \u25ba \n ",
          "source": "el_comercio"
        }
      }
    ]
  }
}

'''