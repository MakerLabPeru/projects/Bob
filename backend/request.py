import requests as req
from web_search import searchArticles

last_name_1 = "TXAPELLIDOPATERNO"
last_name_2 = "TXAPELLIDOMATERNO"
first_names = "TXNOMBRES"
org_politic = "TXORGANIZACIONPOLITICA"
photo       = "TXRUTAARCHIVO"

'''
TIPO (elemento de dict en json de Post)
4 = region / departamento
5 = provincia
6 = distrito
'''
def getTipo(ubigeo):
    tipo = 6
    if   ubigeo[2:6]=='00': tipo -= 2
    elif ubigeo[4:6]=='00': tipo -= 1
    return tipo


# ubigeo = "140104"
def candidates_json(ubigeo):
    r = req.post('https://votoinformado.jne.gob.pe/voto/Hoja/ListCandidato', json = dict(id='',tipo=getTipo(ubigeo), ubi=ubigeo))
    data = []
    for cand_json in r.json():
        candidate = {}
        candidate["name"]           = cand_json[last_name_1] + ' ' + cand_json[last_name_2] + ', ' + cand_json[first_names]
        candidate["org_polit"]      = cand_json[org_politic]
        candidate["url_photo"]      = cand_json[photo]
        candidate["articles"]       = searchArticles([cand_json[last_name_1], cand_json[first_names]])
        data.append(candidate)
    return data


lista = candidates_json("110000")
for candi in lista:
    print("===".center(100,"="))
    print("CANDIDATO: ", candi["name"], " - ", candi["org_polit"])
    print("FOTO: ", candi["url_photo"])
    print("NOTICIAS: ")
    for news in candi["articles"]:
        print("     ", news["title"])
        print("         ", news["url"])

'''
GUIA PARA OBTENER DATOS DE LISTA GENERADA
- name
- org_polit
- url_photo
- articles
    + url
    + title
    + summary
    + thumb_image
'''